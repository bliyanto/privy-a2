console.log('Hello World');
console.error('This is an error');
console.warn('This is a warning');

//var, let, const

let age = 30;
age = 31;
console.log(age);

const birthdate = 10
console.log(birthdate);

//String, Numbers, Boolean, null, undefined, symbol

const name = 'John'; //string
const age2 = 30; //num
const rating = 4.5; //num
const isCool = true; //bool
const x = null;
const y = undefined;
let z; // also undefined

console.log(typeof name);

//Concatenation
console.log('My name is ' + name + ' and I am ' + age);
//Template String
const hello = `My name is ${name} and I am ${age}`;
console.log(hello);

const s = 'Hello World';
console.log(s.length);
console.log(s.toUpperCase());
console.log(s.substring(0, 5).toUpperCase());

const t = 'technology, computers, it, code';
console.log(t.split(', '));

//Arrays
const fruits = ['apples', 'oranges', 'pears'];

fruits[3] = 'grapes';
fruits.push('mangos');
fruits.unshift('strawberries');
fruits.pop();

console.log(Array.isArray(fruits));
console.log(fruits);
console.log(fruits[1]);
console.log(fruits.indexOf('grapes'))

//Object literals
const person = {
    firstName: 'John',
    lastName: 'Doe',
    age: 30,
    hobbies: ['music', 'movies', 'sports'],
    address: {
        street: '50 main st',
        city: 'Boston',
        state: 'MA'
    }
}

console.log(person);
console.log(person.firstName, person.lastName);
const { firstName, lastName, address: { city }} = person;
console.log(city);

const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'Dentist appt',
        isCompleted: true
    },
];

console.log(todos);
console.log(todos[1].text);

const todoJSON = JSON.stringify(todos);
console.log(todoJSON);

// For
for(let i = 0; i < 10; i++) {
    console.log(`For Loop NUmber: ${i}`);
}

for (let i = 0; i < todos.length; i++) {
    console.log(todos[i].text);
}

for(let todo of todos) {
    console.log(todos.text);
}

//While
let i = 0;
while (i < 10) {
    console.log(`While Loop Number: ${i}`);
    i++;
}

//forEach, map, filter
const todoText = todos.map(function(todo) {
    return todo.text;
});
const todoCompleted = todos.filter(function(todo) {
    return todo.isCompleted === true;
}).map(function(todo) {
    return todo.text
});

console.log(todoText);
console.log(todoCompleted);

//Equality and Conditionals
const d = 10;
const k = 4;
if(d === 10 || k < 5) { //triple equal to match data types
    console.log('d is 10 and k is less than 5');
}
else if (d > 10 && k < 5) {
    console.log('d is greater than 10 and k is less than 5')
}
else {
    console.log('d is NOT 10');
}

const l = 10;
let color = 'green';
color = l > 10 ? 'red' : 'blue' //if l is greater than 10 color is red, else it is blue
console.log(color);
switch(color) {
    case 'red':
        console.log('color is red');
        break;
    case 'blue':
        console.log('color is blue');
        break;
    default:
        console.log('color is NOT red or blue');
        break;
}

//Functions

function addNums(num1, num2) {
    console.log(num1 + num2);
}

addNums(5,4);

const subtractNums = (num1, num2) => num1 - num2;
console.log(subtractNums(8, 3));

//Constructor function
function Person(firstName, lastName, dob) {
    // Set object properties
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
}

// Get Birth Year
Person.prototype.getBirthYear = function () {
    return this.dob.getFullYear();
  }
  
// Get Full Name
  Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`
  }
  
  // Instantiate an object from the class
  const person1 = new Person('John', 'Doe', '7-8-80');
  const person2 = new Person('Steve', 'Smith', '8-2-90');
  
  console.log(person2);


//Class
class Persons {
    constructor(firstName, lastName, dob) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.dob = new Date(dob);
    }
  
    // Get Birth Year
    getBirthYear() {
      return this.dob.getFullYear();
    }
  
    // Get Full Name
    getFullName() {
      return `${this.firstName} ${this.lastName}`
    }
  }
  
  const person3 = new Person('John', 'Doe', '7-8-80');
  console.log(person3.getBirthYear());

///////////////////////////////////////////////////////////////////////////////////

console.log(window);
alert(1);

// Single Element Selectors
console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));
// Multiple Element Selectors
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));

// Manipulating UI (DOM)
const ul = document.querySelector('.items');
// ul.remove();
// ul.lastElementChild.remove();
ul.firstElementChild.textContent = 'Hello';
ul.children[1].innerText = 'Brad';
ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

const btn = document.querySelector('.btn');
btn.style.background = 'red';

// EVENTS

// Mouse Event
btn.addEventListener('click', e => {
    e.preventDefault();
    console.log(e.target.className);
    document.getElementById('my-form').style.background = '#ccc';
    document.querySelector('body').classList.add('bg-dark');
    ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
  });
  
  // Keyboard Event
  const nameInput1 = document.querySelector('#name');
  nameInput1.addEventListener('input', e => {
    document.querySelector('.container').append(nameInput1.value);
  });
  
  
  // USER FORM SCRIPT
  
  // Put DOM elements into variables
  const myForm = document.querySelector('#my-form');
  const nameInput = document.querySelector('#name');
  const emailInput = document.querySelector('#email');
  const msg = document.querySelector('.msg');
  const userList = document.querySelector('#users');
  
  // Listen for form submit
  myForm.addEventListener('submit', onSubmit);
  
  function onSubmit(e) {
    e.preventDefault();
    
    if(nameInput.value === '' || emailInput.value === '') {
      // alert('Please enter all fields');
      msg.classList.add('error');
      msg.innerHTML = 'Please enter all fields';
  
      // Remove error after 3 seconds
      setTimeout(() => msg.remove(), 3000);
    } else {
      // Create new list item with user
      const li = document.createElement('li');
  
      // Add text node with input values
      li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));
  
      // Add HTML
      // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;
  
      // Append to ul
      userList.appendChild(li);
  
      // Clear fields
      nameInput.value = '';
      emailInput.value = '';
    }
  }